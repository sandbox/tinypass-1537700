
Install the latest TinyPass 2.x API (tinypass-php-sdk.2.0.x.zip).   
The API zip file can be downloaded
http://developer.tinypass.com/_media/start/tinypass-php-sdk-2.0.x.zip
or
http://developer.tinypass.com

Once downloaded, unzip the entire contents into this api directory.
It should look like:
  modules/tinypass/api/TinyPass.php
  modules/tinypass/api/TinyPassGateway.php
  ...
  ...
  modules/tinypass/api/.. - and more files




For more information:
	http://developer.tinypass.com
