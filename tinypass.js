var tinypass = {

  showMeteredOptions: function(elem){
    var elem = jQuery(elem);
    var type = elem.val()

    jQuery(".tp-metered-options").hide();
    jQuery(".tp-metered-options :input").attr('disabled', 'disabled')
    jQuery("#tp-metered-" + type).show('medium');
    jQuery("#tp-metered-" + type + " :input").removeAttr('disabled');

  },

  tp_showOption: function(elem){
    var id = jQuery(elem).attr("id");
    var i = id.substring(id.length - 1);
    jQuery('#tp_options' + i).toggle(jQuery(elem).attr('checked'));
  },

  clearTimes: function(i){
    jQuery("#edit-tp-start-time" + i + "-datepicker-popup-0").val("");
    jQuery("#edit-tp-start-time" + i + "-timeEntry-popup-1").val("");
    jQuery("#edit-tp-end-time" + i + "-datepicker-popup-0").val("");
    jQuery("#edit-tp-end-time" + i + "-timeEntry-popup-1").val("");
  },

  log:function(msg){
    if(console && console.log) {
      console.log(msg);
    }
  },

  addPriceOption: function(){
    var count = jQuery(".tp-price-option-form:visible").size();
    if(count <= 3){
      var opt = count;
      jQuery("#tp-options" + opt).show('fast');
      jQuery("#tp-options" + opt).find("input:hidden").val("1");
    }
  },
  removePriceOption: function(){
    var count = jQuery(".tp-price-option-form:visible").size();
    if(count > 1) {
      var opt = count - 1;
      jQuery("#tp-options" + opt).hide('fast');
      jQuery("#tp-options" + opt).find("input:hidden").val("0");
    }
  }
}

$(function(){

  if ($("#edit-tp-enable-for-node").attr("checked") == false) {
    $("#tp_node_options").hide();
  }

  $("#edit-tp-enable-for-node-link").bind("click", function() {

    if($(this).attr("state")){
      $(this).removeAttr("state");
      $("#tp_node_options").slideUp('fast');
    }else{
      $(this).attr("state", 1)
      $("#tp_node_options").slideDown('fast');
    }
  });

  $("#edit-tp-enable-for-node").bind("click", function() {
    if ($(this).attr("checked")) {
      $("#tp_node_options").slideDown('fast');
    } else {
      $("#tp_node_options").slideUp('fast');
    }
  });


  $(".tinypass_option_enabled").bind("change", function(arg1, arg2){
    var id = arg1.target.id;
    var i = id.substring(id.length - 1);
    tinypass.tp_showOption('tp_options' + i, arg1.target.checked);
  });

  tinypass.showMeteredOptions(document.getElementsByName("metered")[0])


})
