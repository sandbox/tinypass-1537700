<?php

/**
 * @file
 * TinyPass configuration settings
 */

/**
 * Form builder; Configure the Tinypass system.
 *
 * @see system_settings_form()
 */
function tinypass_admin_settings() {

  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['general']['tinypass_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Tinypass'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => variable_get('tinypass_enabled', '1'),
  );

  $form['general']['tinypass_aid_sand'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID (Sandbox)'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => variable_get('tinypass_aid_sand', 'W7JZEZFu2h'),
    '#description' => t('Application ID that identifies your client application.  Retreived from your Tinypass publisher account'),
  );

  $form['general']['tinypass_secret_key_sand'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key (Sandbox)'),
    '#size' => 40,
    '#maxlength' => 40,
    '#default_value' => variable_get('tinypass_secret_key_sand', 'jeZC9ykDfvW6rXR8ZuO3EOkg9HaKFr90ERgEb3RW'),
    '#description' => t('Tinypass Application ID that identifies your client application.  Retreived from your Tinypass publisher account'),
  );


  $form['general']['tinypass_aid_prod'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID (Live)'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => variable_get('tinypass_aid_prod', 'AIDAIDAID1'),
    '#description' => t('Tinypass Application ID that identifies your client application.  Retreived from your Tinypass publisher account'),
  );

  $form['general']['tinypass_secret_key_prod'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key (Live)'),
    '#size' => 40,
    '#maxlength' => 40,
    '#default_value' => variable_get('tinypass_secret_key_prod', 'Get From http://www.tinypass.com'),
    '#description' => t('Tinypass Application ID that identifies your client application.  Retreived from your Tinypass publisher account'),
  );


  $form['general']['tinypass_env'] = array(
    '#type' => 'radios',
    '#title' => t('Environment'),
    '#default_value' => variable_get('tinypass_env', 1),
    '#options' => array(t('Sandbox'), t("Live")),
    '#description' => t('Testing?  Use the sandbox environment'),
  );

  $form['general']['tinypass_access_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Access Message'),
    '#default_value' => variable_get('tinypass_access_message', t("To continue, please purchase using Tinypass")),
    '#description' => t('Message to display when content access is denied'),
  );

  $form['general']['tinypass_access_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Access Message'),
    '#default_value' => variable_get('tinypass_access_message', t("To continue, please purchase using Tinypass")),
    '#description' => t('Message to display when content access is denied'),
  );

  $form['general']['help'] = array(
    '#type' => 'markup',
    '#value' => '<p>Access Message can be fully customized by defining a <b>tinypass_denied_message.tpl.php</b> template</p>',
  );


  return system_settings_form($form);
}

/**
 * Validates tinypass system settings.
 */
function tinypass_admin_settings_validate($form, &$form_state) {
  if ($form_state['values']['tinypass_enabled'] == 1) {
    if (tinypass_load() == FALSE) {
      form_set_error('tinypass_enabled', t('Tinypass PHP API has not been successfully installed. <p>Please download the latest version of the PHP and unzip the contents into the modules/tinypass/api directory</p>'));
      form_set_value($form['general']['tinypass_enabled'], 0, $form_state);
    }
  }
}
