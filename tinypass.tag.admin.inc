<?php

/**
 * @file
 * Provides methods for creating and modifying tinypass enabled tags
 */

/**
 * Process the tag list page.
 */
function tinypass_tag_list_page() {
  $result = db_query("SELECT tm.*, term.name, term.tid FROM {tinypass_tag_meta} tm, {vocabulary} v, {term_data} term WHERE v.vid = term.vid AND tm.tid = term.tid ");
  $rows = array();

  while ($row = db_fetch_array($result)) {
    $entry = array();
    $entry['term'] = $row['name'];
    $entry['tid'] = $row['tid'];
    $entry['meta_id'] = $row['meta_id'];
    $entry['details'] = '';

    $entry['enabled'] = t('no');
    if ($row['enabled']) {
      $entry['enabled'] = t('yes');
    }

    $meta = unserialize($row['data']);

    $entry['resource_name'] = isset($meta['resource_name']) ? $meta['resource_name'] : '';

    $options = new TinyPassOptions($meta, 1);

    $entry['details'] = tinypass_options_overview($options);
    $rows[] = $entry;
  }

  drupal_set_title(t("TinyPass enabled terms"));
  $output = theme('tinypass_tag_list_display', $rows);
  return $output;
}

/**
 * Validate tag edit form.
 */
function tinypass_edit_tag_form_validate($form, &$form_state) {
  _tinypass_parse_meta_data($form, $form_state);
}

/**
 * Parse the tinypass meta data options.
 */
function _tinypass_parse_meta_data(&$form, &$form_state) {

  tinypass_load();

  $data = array();
  $pos = array();
  $post_data = $form['#post'];

  $data['enabled'] = isset($post_data['tp_enable_for_tag']) ? $post_data['tp_enable_for_tag'] : "0";
  $data['meta_id'] = $post_data['meta_id'];

  $meta = array();
  $meta['resource_name'] = $post_data['tp_resource_name'];

  // Metered configuration.
  if ($post_data['metered'] == 'off') {
    $meta['metered'] = 'off';
  }
  elseif ($post_data['metered'] == 'time') {
    $meta['metered'] = 'time';
    $meta['m_tp'] = $post_data['m_tp'];
    $meta['m_tp_type'] = $post_data['m_tp_type'];
    $meta['m_lp'] = $post_data['m_lp'];
    $meta['m_lp_type'] = $post_data['m_lp_type'];

    if ($meta['m_tp'] == '') {
      form_set_error("m_tp", t('Trial period cannot be empty'));
    }

    if (!is_numeric($meta['m_tp'])) {
      form_set_error("m_tp", t('Trial period must be a valid number'));
    }

    if ($meta['m_lp'] == '') {
      form_set_error("m_lp", t('Lockout period cannot be empty'));
    }

    if (!is_numeric($meta['m_lp'])) {
      form_set_error("m_lp", t('Lockout period must be a valid number'));
    }
  }
  else {

    $meta['metered'] = 'count';
    $meta['m_maa'] = $post_data['m_maa'];
    $meta['m_lp'] = $post_data['m_lp'];
    $meta['m_lp_type'] = $post_data['m_lp_type'];

    if ($meta['m_maa'] == '') {
      form_set_error("m_maa", t('Max views cannot be empty'));
    }

    if (!is_numeric($meta['m_maa'])) {
      form_set_error("m_maa", t('Max views must be a valid number'));
    }

    if ($meta['m_lp'] == '') {
      form_set_error("m_lp", t('Lockout period cannot be empty'));
    }

    if (!is_numeric($meta['m_lp'])) {
      form_set_error("m_lp", t('Lockout period must be a valid number'));
    }
  }

  // Price options values.
  $post_data["tp_option_enabled0"] = "1";
  for ($i = 0; $i < 3; $i++) {

    $podata['en'] = (isset($post_data["tp_option_enabled$i"])) ? $post_data["tp_option_enabled$i"] : "0";

    if ($podata['en'] == "1") {

      $podata['p'] = $post_data["tp_price$i"];
      if (!TPValidate::validatePrice($podata['p'])) {
        form_set_error("tp_price$i", t(TPValidate::PRICE_FAILED_MSG));
      }

      $podata['ap'] = $post_data["tp_access_period$i"];
      $podata['apt'] = $post_data["tp_access_period_type$i"];

      if (!TPValidate::validateAccessPeriod($podata['ap'])) {
        form_set_error("tp_access_period$i", t(TPValidate::ACCESS_PERIOD_FAILED_MSG));
      }
      $podata['cpt'] = trim($post_data["tp_caption$i"]);

      _tinypass_unset_if_empty($podata, 'cpt');
      _tinypass_unset_if_empty($podata, 'st');
      _tinypass_unset_if_empty($podata, 'et');

      $pos[$i] = $podata;
    }
  }

  $meta['pos'] = $pos;

  $data['meta'] = $meta;


  $term_text = $post_data['term'];
  $term_text = preg_replace('/\(\d*\)/', '', $term_text);
  $values = preg_split('/\//', $term_text);

  if (count($values) != 2) {
    form_set_error("term", "Invalid term/taxonomy value for '$term_text'");
  }
  else {
    $data['vname'] = $values[0];
    $data['tname'] = $values[1];
  }

  $result = db_query("SELECT term.tid FROM {term_data} term, {vocabulary} v WHERE v.vid = term.vid AND v.name = '%s' AND term.name = '%s'", $data['vname'], $data['tname']);
  $row = db_fetch_array($result);
  $data['tid'] = $row['tid'];

  return $data;
}

/**
 * Process the edit tag form when on submit.
 */
function tinypass_edit_tag_form_submit($form, &$form_state) {

  $data = _tinypass_parse_meta_data($form, $form_state);

  $record = array();
  $record['tid'] = $data['tid'];
  $record['enabled'] = $data['enabled'];
  $record['data'] = serialize($data['meta']);
  $record['meta_id'] = $data['meta_id'];

  $update = array();
  if (isset($record['meta_id']) && $record['meta_id'] > 0) {
    $update = 'meta_id';
  }

  drupal_write_record("tinypass_tag_meta", $record, $update);

  drupal_set_message(t("Tag details '@term' saved", array("@term" => $data['meta']['resource_name'])));

  $form_state['redirect'] = 'admin/content/tinypass/tag_list';

  return;
}

/**
 * Edit existing tag configuration.
 */
function tinypass_edit_tag_page($id) {
  $result = db_fetch_array(db_query('SELECT * FROM {tinypass_tag_meta} WHERE meta_id = %d ', $id));
  $result['data'] = unserialize($result['data']);
  return drupal_get_form('tinypass_edit_tag_form', $result);
}

/**
 * Construct edit tag form.
 */
function tinypass_edit_tag_form(&$form_state, $form_data = NULL, $data = array()) {

  drupal_add_js(drupal_get_path('module', 'tinypass') . '/tinypass.js');
  drupal_add_css(drupal_get_path('module', 'tinypass') . '/tinypass.css');

  $data = array();
  if (is_array($form_data)) {
    $data = $form_data;
  }

  $form = array();
  $meta = array();

  $enabled = '1';
  $term = 1;
  $tid = -1;
  $meta_id = '';
  $resource_name = '';

  $metered = 'off';

  $lockout_period = '';
  $lockout_period_type = '';

  $trial_period = '';
  $trial_period_type = '';

  $view_limit = '';

  if (isset($data['data'])) {
    $meta = $data['data'];
  }

  if (isset($data['enabled'])) {
    $enabled = $data['enabled'];
  }

  if (isset($data['tid'])) {
    $tid = $data['tid'];
  }

  if (isset($data['meta_id'])) {
    $meta_id = $data['meta_id'];
  }

  if (isset($meta['resource_name'])) {
    $resource_name = $meta['resource_name'];
  }

  if (isset($meta['m_lp'])) {
    $lockout_period = $meta['m_lp'];
  }

  if (isset($meta['m_maa'])) {
    $view_limit = $meta['m_maa'];
  }

  if (isset($meta['m_lp_type'])) {
    $lockout_period_type = $meta['m_lp_type'];
  }

  if (isset($meta['m_tp'])) {
    $trial_period = $meta['m_tp'];
  }

  if (isset($meta['m_tp_type'])) {
    $trial_period_type = $meta['m_tp_type'];
  }

  $vars = array();
  $vars['tp_metered_count_class'] = 'hide';
  $vars['tp_metered_time_class'] = 'hide';
  if (isset($meta['metered'])) {

    $metered = $meta['metered'];

    if ($metered == 'count') {
      $vars['tp_metered_count_class'] = '';
      $vars['tp_metered_time_class'] = 'hide';
    }
    elseif ($metered == 'time') {
      $vars['tp_metered_count_class'] = 'hide';
      $vars['tp_metered_time_class'] = '';
    }
  }
  $form['#vars'] = $vars;


  $times = array(
    'minute' => 'minute(s)',
    'hour' => 'hour(s)',
    'day' => 'day(s)',
    'week' => 'week(s)',
    'month' => 'month(s)',
  );

  $result = db_query("SELECT t.tid, t.name, v.name vname FROM {term_data} t, {vocabulary} v WHERE t.vid = v.vid AND t.tid = '%d' ", $tid);

  $term = '';
  if ($result->num_rows) {
    $row = db_fetch_array($result);
    $term = $row['vname'] . '/' . $row['name'] . "(" . $row['tid'] . ")";
  }


  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  $form['tinypass'] = array(
    '#type' => 'fieldset',
    '#title' => t('Details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 30,
  );

  $form['tinypass']['tp_enable_for_tag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable TinyPass for this term'),
    '#default_value' => $enabled,
    '#description' => 'Disabling will cause TinyPass to be skipped',
    '#weight' => -1,
  );

  $form['tinypass']['term'] = array(
    '#type' => 'textfield',
    '#title' => t('Term'),
    '#autocomplete_path' => 'admin/tinypass/taxonomy_autocomplete',
    '#default_value' => $term,
    '#description' => t('TinyPass will be enabled for all content that has been assigned the following term.  <b>Only one term may be selected</b>'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['tinypass']['tp_resource_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Display name'),
    '#default_value' => $resource_name,
    '#description' => t('This will be displayed on the TinyPass popup - This is the friendly name of what your users are purchasing'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 60,
  );

  $form['tinypass']['metered'] = array(
    '#type' => 'select',
    '#default_value' => $metered,
    '#attributes' => array(
      'onchange' => 'tinypass.showMeteredOptions(this)'),
    '#options' => array(
      'off' => 'Disabled',
      'count' => 'View Based',
      'time' => 'Time Based',
    ),
  );


  // View Limit.
  $form['tinypass']['m_maa'] = array(
    '#type' => 'textfield',
    '#default_value' => $view_limit,
    '#maxlength' => 5,
    '#size' => 5,
  );

  // Lockout Period.
  $form['tinypass']['m_lp'] = array(
    '#type' => 'textfield',
    '#default_value' => $lockout_period,
    '#maxlength' => 5,
    '#size' => 5,
  );

  // Lockout Period.
  $form['tinypass']['m_lp_type'] = array(
    '#type' => 'select',
    '#default_value' => $lockout_period_type,
    '#options' => $times,
  );

  // Trial Period.
  $form['tinypass']['m_tp'] = array(
    '#type' => 'textfield',
    '#default_value' => $trial_period,
    '#maxlength' => 5,
    '#size' => 5,
  );

  // Trial Period Type.
  $form['tinypass']['m_tp_type'] = array(
    '#type' => 'select',
    '#default_value' => $trial_period_type,
    '#options' => $times,
  );


  $form['tinypass']['meta_id'] = array(
    '#type' => 'hidden',
    '#value' => $meta_id,
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'cancel',
    '#attributes' => array('onClick' => 'history.go(-1);return false;'),
  );


  $pos = array();
  if (isset($meta['pos'])) {
    $pos = $meta['pos'];
  }
  for ($i = 0; $i < 3; $i++) {

    $price = '';
    $access_period = '';
    $caption = '';
    $apt = '';

    if (isset($pos[$i])) {
      if (isset($pos[$i]['p'])) {
        $price = $pos[$i]['p'];
      }

      if (isset($pos[$i]['ap'])) {
        $access_period = $pos[$i]['ap'];
      }

      if (isset($pos[$i]['apt'])) {
        $apt = $pos[$i]['apt'];
      }

      if (isset($pos[$i]['cpt'])) {
        $caption = $pos[$i]['cpt'];
      }
    }

    if ($i > 0) {
      $form['tinypass']['tp_option_enabled' . $i] = array(
        '#type' => 'hidden',
        '#default_value' => $price != '' && $price > 0 ? TRUE : FALSE,
        '#title' => check_plain("Price Option" . ($i + 1)),
      );
    }

    $form['tinypass']['tp_price' . $i] = array(
      '#type' => 'textfield',
      '#title' => t("Price"),
      '#default_value' => $price,
      '#maxlength' => 8,
      '#size' => 8,
      '#description' => t(''),
    );

    $form['tinypass']['tp_access_period' . $i] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => $access_period,
    );

    $form['tinypass']['tp_access_period_type' . $i] = array(
      '#type' => 'select',
      '#default_value' => $apt,
      '#options' => array(
        'hour' => 'hour(s)',
        'day' => 'day(s)',
        'week' => 'week(s)',
        'month' => 'month(s)',
        'year' => 'year(s)',
      ),
    );


    $form['tinypass']['tp_caption' . $i] = array(
      '#type' => 'textfield',
      '#title' => t("Custom Caption"),
      '#default_value' => $caption,
      '#maxlength' => 30,
      '#size' => 30,
      '#description' => t(''),
    );
  }

  return $form;
}

/**
 * Delete tag configuration.
 */
function tinypass_remove_tag($meta_id) {
  db_query("DELETE FROM {tinypass_tag_meta} WHERE meta_id = '%d'", $meta_id);
  drupal_redirect_form(array(), url('admin/content/tinypass/tag_list'));
}

/**
 * Helper function for autocompletion.
 */
function tinypass_taxonomy_autocomplete($st = '') {

  // Fetch last tag.
  $x = drupal_explode_tags(trim($st));
  $last_string = trim(array_pop($x));
  $matches = array();
  if ($last_string != '') {
    $result = db_query_range(db_rewrite_sql("SELECT t.tid, t.name, v.name vname FROM {term_data} t, {vocabulary} v WHERE t.vid = v.vid AND t.vid >= 0 AND LOWER(t.name) LIKE LOWER('%%%s%%')", 't', 'tid'), $last_string, 0, 10);

    while ($tag = db_fetch_object($result)) {
      $n = $tag->name;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (strpos($tag->name, ',') !== FALSE || strpos($tag->name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $tag->name) . '"';
      }
      $matches[$tag->vname . "/" . $tag->name . "(" . $tag->tid . ")"] = check_plain($tag->name);
    }
  }

  drupal_json($matches);
}
