<?php

/**
 * @file
 * Contains support classes and methods for TinyPass module
 */

/**
 * Options Helper Class used generically across PHP plugins
 */
class TinyPassOptions {

  /**
   * Constructor.
   */
  public function __construct($data, $enabled = FALSE) {

    $this->data = $data;

    $this->data['en'] = $enabled;

    $this->offer_priority = isset($data['offer_priority']) && 'secondary' == $data['offer_priority'] ? 'secondary' : 'primary';

    $this->resource_id = isset($data['resource_id']) ? $data['resource_id'] : '';

    $this->resource_name = isset($data['resource_name']) ? $data['resource_name'] : '';

    $this->num_prices = count($data['pos']);
  }

  /**
   * IsEnabled.
   */
  public function isEnabled() {
    return $this->is('en');
  }

  /**
   * Helper function.
   */
  protected function is($field) {
    return !empty($this->data[$field]);
  }

  /**
   * Retreive offer priority.
   */
  public function getOfferPriority() {
    return $this->offer_priority;
  }

  /**
   * Get the resource name.
   */
  public function getResourceName() {
    return $this->resource_name;
  }

  /**
   * Set the resource name.
   */
  public function setResourceName($s) {
    $this->resource_name = $s;
  }

  /**
   * Get the resource ID.
   */
  public function getResourceId() {
    return $this->resource_id;
  }

  /**
   * Set the resource ID.
   */
  public function setResourceId($s) {
    $this->resource_id = $s;
  }

  /**
   * Get the number of configured POs.
   */
  public function getNumPrices() {
    return $this->num_prices;
  }

  /**
   * Get a price value a the specified index.
   */
  protected function getPOValue($index, $field, $default_value) {
    if (isset($this->data['pos'][$index][$field])) {
      return $this->data['pos'][$index][$field];
    }

    return $default_value;
  }

  /**
   * Get the price at the specified index.
   */
  public function getPrice($i) {
    return $this->getPOValue($i, "p", '');
  }

  /**
   * Get the access string at the specified index.
   */
  public function getAccess($i) {
    $ap = $this->getPOValue($i, "ap", '');
    $apt = $this->getPOValue($i, "apt", '');

    if ($ap == '' || $apt == '') {
      return '';
    }

    return $ap . " " . $apt;
  }

  /**
   * Get the caption at the specified index.
   */
  public function getCaption($i) {
    return $this->getPOValue($i, "cpt", '');
  }

  /**
   * Get the start date at the specified index.
   */
  public function getStartDateSec($i) {
    return $this->getPOValue($i, "st", '');
  }

  /**
   * Get the end date at the specified index.
   */
  public function getEndDateSec($i) {
    return $this->getPOValue($i, "et", '');
  }

  /**
   * Is options metered.
   */
  public function isMetered() {
    if ($this->is('metered')) {
      return in_array($this->data['metered'], array('count', 'time'));
    }
    return FALSE;
  }

  /**
   * Is options metered by time.
   */
  public function isTimeMetered() {
    return $this->is('metered') && $this->data['metered'] == 'time';
  }

  /**
   * Is options metered by count.
   */
  public function isCountMetered() {
    return $this->is('metered') && $this->data['metered'] == 'count';
  }

  /**
   * Get the lockout period.
   */
  public function getLockoutPeriod() {
    return $this->data["m_lp"] . " " . $this->data["m_lp_type"];
  }

  /**
   * Get the max access attempts.
   */
  public function getMaxAccessAttempts() {
    return $this->data["m_maa"];
  }

  /**
   * Get the trial period.
   */
  public function getTrialPeriod() {
    return $this->data["m_tp"] . " " . $this->data['m_tp_type'];
  }

}

/**
 * Create an offer from TP options.
 */
function _tinypass_create_offer(TinyPassOptions $options) {
  if ($options == NULL) {
    return NULL;
  }

  $resource = new TPResource($options->getResourceId(), $options->getResourceName());

  $pricing = TPPricingPolicy::createBasic();

  if ($options->isEnabled()) {
    for ($i = 0; $i < $options->getNumPrices(); $i++) {

      $po = new TPPriceOption($options->getPrice($i));

      if ($options->getAccess($i) != '') {
        $po->setAccessPeriod($options->getAccess($i));
      }

      if ($options->getCaption($i) != '') {
        $po->setCaption($options->getCaption($i));
      }

      if ($options->getStartDateSec($i) != '') {
        $po->setStartDateInSecs(strtotime($options->getStartDateSec($i)) - (variable_get('date_default_timezone', 0)));
      }

      if ($options->getEndDateSec($i) != '') {
        $po->setEndDateInSecs(strtotime($options->getEndDateSec($i)) - (variable_get('date_default_timezone', 0)));
      }

      $pricing->addPriceOption($po);
    }
  }

  $offer = new TPOffer($resource, $pricing);

  return $offer;
}

/**
 * Helper function to unset if empty from an array.
 */
function _tinypass_unset_if_empty(&$arr, $field) {
  if (isset($arr[$field])) {
    if ($arr[$field] == '') {
      unset($arr[$field]);
    }
  }
}

/**
 * Load TinyPass external api files.
 */
function tinypass_load() {
  if (file_exists(__DIR__ . '/api/TinyPass.php')) {
    include_once 'api/TinyPass.php';
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper method to debug objects.
 */
function tinypass_debug($object) {
  echo "<pre>";
  print_r($object);
  echo "</pre>";
}
