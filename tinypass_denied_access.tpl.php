<?php
/**
 * @file
 * Template for displaying TinyPass button and messagning when access is denied
 */
/**
 * Template variables
 *
 * $button_html - the tinypass button
 * $access_message - the message provided in the TinyPass Configuration
 */
?>
<br/>
<div class="tinypass_button_holder">
  <div class="tinypass_access_message"><?php echo $access_message ?></div>
  <?php echo $button_html ?>
</div>
