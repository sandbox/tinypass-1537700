<?php
/**
 * @file
 * TinyPass node options form
 */
?>

<?php
$st = 'style="display:none"';
echo drupal_render($form['tp_enable_for_node']);
echo '<a href="#" onclick="return false;" id="edit-tp-enable-for-node-link">Show/Hide Details</a>';
?>

<style>
  #tp_node_options .form-item {
    margin-top:0px;
    margin-bottom:0px;
  }
</style>

<div id="tp_node_options" <?php echo $st; ?>>
  <?php echo drupal_render($form['tp_resource_id']) ?>
  <?php echo drupal_render($form['tp_resource_name']) ?>
  <?php echo drupal_render($form['tp_meta_id']) ?>
  <table class="tinypass_price_options">
    <tr>
      <th><?php echo t('Pricing Options'); ?>
        -
        <a id="tp_add_price_option" style="display:inline" onclick="tinypass.addPriceOption();return false;" href="#">Add</a>
        /
        <a id="tp_remove_price_option" style="display:inline" onclick="tinypass.removePriceOption();return false;" href="#">Remove</a>
        - <?php echo t("(between 1 and 3)") ?>
      </th>
    </tr>
    <?php for ($i = 0; $i < 3; $i++): ?>
      <tr>
        <?php
        $style = '';
        if ($i > 0) :
          $enabled = $form['tp_option_enabled' . $i]['#value'];
          $style = $enabled ? 'display:block' : 'display:none';
        endif;
        ?>
        <td >
          <div id="tp-options<?php echo $i ?>" style="<?php echo $style ?>" class="tp-price-option-form">

            <?php echo drupal_render($form['tp_option_enabled' . $i]) ?>
            <div>
              <table class="tp_add_tag_table">
                <tr>
                  <td>
                    <?php echo drupal_render($form['tp_price' . $i]) ?>
                  </td>
                  <td>
                    <table class="no-border" style="margin:0px">
                      <tr>
                        <td colspan="2" style="padding:0px" class="form-item"><label><?php echo t("Access Period:") ?></label></td>
                      </tr>
                      <tr>
                        <td style="padding:0px" width=50>
                          <?php echo drupal_render($form['tp_access_period' . $i]); ?>
                        </td>
                        <td style="padding:0px">
                          <?php echo drupal_render($form['tp_access_period_type' . $i]); ?>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <?php echo drupal_render($form['tp_caption' . $i]) ?>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </td>
      </tr>
    <?php endfor; ?>
  </table>
</div>
