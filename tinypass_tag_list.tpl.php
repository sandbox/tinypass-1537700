<?php
/**
 * @file
 * Template for displaying list of TinyPass enabled terms
 */
?>
<p>
  <?php echo l(t("Configure a new tag"), 'admin/content/tinypass/create_tag'); ?>
</p>

<table>

  <thead>
    <tr>
      <th width="30px">ID</th>
      <th width="30px"><?php echo t("Enabled"); ?></th>
      <th width="100px"><?php echo t("Term"); ?></th>
      <th><?php echo t("Options"); ?></th>
      <th width="30px">Operations</th>
  </thead>
  <tbody>

    <?php
    $i = 0;
    foreach ($rows as $row) :

      $oe = ($i++ % 2 == 0) ? "odd" : "even";

      echo "<tr class='$oe'>";
      echo "<td>{$row['meta_id']}</td>";
      echo "<td>{$row['enabled']}</td>";
      echo "<td>" . l($row['term'], "taxonomy/term/" . $row['tid']) . "</td>";
      echo "<td>{$row['details']}</td>";
      echo "<td><a href='" . url("admin/content/tinypass/") . "{$row['meta_id']}/edit_tag" . "'>" . t("edit") . "</a>";
      echo "&nbsp; <a onclick=\"return confirm('" . t("Are you sure you want to remove these settings") . "');\" href='" . url("admin/content/tinypass/") . "{$row['meta_id']}/remove_tag" . "'>" . t('remove') . "</a></td>";
      echo "</tr>";

    endforeach
    ?>

  </tbody>
</table>
